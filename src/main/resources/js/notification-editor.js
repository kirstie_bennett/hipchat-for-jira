(function($) {

    var NotificationEditor = Class.extend({

        init: function (options) {
            var instance = this;
            instance.$editor = options.editor;
            instance.$toolbar = options.toolbar;

            instance.$toolbar.find(".aui-button")
                .tooltip()
                .click(function(e) {
                    e.preventDefault();
                    $(this).tooltip("hide");
                    var macro = $(this).attr("data-macro");
                    if (macro) {
                        instance.insertMacro("$" + macro);
                        instance.$editor.trigger("contentModified");
                        instance.$editor.focus();
                    }
                });
        },

        /**
         * Insert the macro text at the current cursor position in the editor.
         *
         * Utility methods borrowed from Mention.js in the JIRA source.
         */
        insertMacro: function(macro) {
            var raw = this._rawInputValue(),
                caretPos = this._getCaretPosition(),
                before = raw.substr(0, caretPos).replace(/\r\n/g, "\n"),
                after = raw.substr(caretPos);

            if (before.length !== 0) {
                macro = " " + macro;
            }

            this._rawInputValue([before, macro, after].join(""));
            this._setCursorPosition(before.length + macro.length);
        },

        /**
         * Sets the cursor position to the specified index.
         *
         * @param index The index to move the cursor to.
         */
        _setCursorPosition: function(index) {
            var input = this.$editor.get(0);
            if (input.setSelectionRange) {
                input.focus();
                input.setSelectionRange(index, index);
            } else if (input.createTextRange) {
                var range = input.createTextRange();
                range.collapse(true);
                range.moveEnd('character', index);
                range.moveStart('character', index);
                range.select();
            }
        },

        /**
         * Returns the position of the cursor in the editor field.
         */
        _getCaretPosition:function (){

            var element = this.$editor.get(0);
            var rawElementValue = this._rawInputValue();
            var caretPosition, range, offset, normalizedElementValue, elementRange;

            if (typeof element.selectionStart === "number") {
                return element.selectionStart;
            }

            if (document.selection && element.createTextRange){
                range = document.selection.createRange();
                if (range) {
                    elementRange = element.createTextRange();
                    elementRange.moveToBookmark(range.getBookmark());

                    if (elementRange.compareEndPoints("StartToEnd", element.createTextRange()) >= 0) {
                        return rawElementValue.length;
                    } else {
                        normalizedElementValue = rawElementValue.replace(/\r\n/g, "\n");
                        offset = elementRange.moveStart("character", -rawElementValue.length);
                        caretPosition = normalizedElementValue.slice(0, -offset).split("\n").length - 1;
                        return caretPosition - offset;
                    }
                }
                else {
                    return rawElementValue.length
                }
            }
            return 0;
        },

        /**
         * Gets or sets the text value of our input via the browser, not jQuery.
         * @return The precise value of the input element as provided by the browser (and OS).
         * @private
         */
        _rawInputValue: function() {
            var el = this.$editor.get(0);
            if (typeof arguments[0] == "string") el.value = arguments[0];
            return el.value;
        }
    });

    AJS.toInit(function() {
        var $editor = $(".hipchat-notification-editor");
        if ($editor.length) {
            new NotificationEditor({
                editor: $editor,
                toolbar: $(".hipchat-notification-editor-toolbar")
            });
        }
    });
})(AJS.$);