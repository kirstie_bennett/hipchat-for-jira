package com.atlassian.labs.hipchat.contextproviders;

import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

/**
 * Context provider for the web panel on the project summary page.
 */
public class HipChatWebPanelContextProvider implements CacheableContextProvider
{
    private static final String HAS_HIP_CHAT_API_TOKEN = "hasHipChatApiToken";
    private final ConfigurationManager configurationManager;

    public HipChatWebPanelContextProvider(ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
    }

    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
    }

    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        final MapBuilder<String, Object> contextMap = MapBuilder.<String, Object>newBuilder().addAll(context);
        contextMap.add(HAS_HIP_CHAT_API_TOKEN, hasHipChatApiToken());
        return contextMap.toMap();
    }

    private boolean hasHipChatApiToken()
    {
        final String hipChatApiToken = configurationManager.getHipChatApiToken();
        return hipChatApiToken != null && !hipChatApiToken.equals("");
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context)
    {
        return getClass().getName();
    }
}
