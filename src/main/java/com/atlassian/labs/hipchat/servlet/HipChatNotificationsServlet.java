package com.atlassian.labs.hipchat.servlet;

import com.atlassian.fugue.Either;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.client.Room;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.VelocityParamFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.labs.hipchat.components.ConfigurationManager;
import com.atlassian.labs.jira.dto.dummy.DummyNotificationDto;
import com.atlassian.labs.jira.notification.HipChatMessageRenderer;
import com.atlassian.labs.jira.workflow.HipChatPostFunction;
import com.atlassian.labs.jira.workflow.HipChatPostFunctionFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HipChatNotificationsServlet extends HttpServlet
{
    /**
     * Matches "/hipchat/notifications/(projectkey)"
     */
    private static final Pattern PATTERN = Pattern.compile("/(.+)?");

    private static final String POST_FUNCTIONS_KEY_NAME = "postFunctions";
    private static final String HAS_HIP_CHAT_API_TOKEN = "hasHipChatApiToken";
    private static final String CLASS_NAME_ARG_KEY = "class.name";

    private final JiraAuthenticationContext authenticationContext;
    private final TemplateRenderer renderer;
    private final ProjectService projectService;
    private final VelocityParamFactory velocityParamFactory;
    private final IssueTypeManager issueTypeManager;
    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final HipChatClient hipChatClient;
    private final HipChatMessageRenderer messageRenderer;
    private final ApplicationProperties applicationProperties;
    private final ConfigurationManager configurationManager;

    public HipChatNotificationsServlet(final JiraAuthenticationContext authenticationContext,
                                       final TemplateRenderer renderer, final ProjectService projectService,
                                       final VelocityParamFactory velocityParamFactory,
                                       final WorkflowSchemeManager workflowSchemeManager,
                                       final IssueTypeManager issueTypeManager, final WorkflowManager workflowManager,
                                       final HipChatClient hipChatClient, final HipChatMessageRenderer messageRenderer,
                                       final ApplicationProperties applicationProperties,
                                       final ConfigurationManager configurationManager)
    {
        this.authenticationContext = authenticationContext;
        this.renderer = renderer;
        this.projectService = projectService;
        this.velocityParamFactory = velocityParamFactory;
        this.issueTypeManager = issueTypeManager;
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
        this.hipChatClient = hipChatClient;
        this.messageRenderer = messageRenderer;
        this.applicationProperties = applicationProperties;
        this.configurationManager = configurationManager;
    }

    @Override
    public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
    {
        final Matcher matcher = PATTERN.matcher(request.getPathInfo());
        if (matcher.matches())
        {
            String projectKey = matcher.group(1);

            final ApplicationUser user = authenticationContext.getUser();
            ProjectService.GetProjectResult result = projectService.getProjectByKeyForAction(user, projectKey, ProjectAction.EDIT_PROJECT_CONFIG);

            if (result.isValid())
            {
                Project project = result.getProject();

                final Map<String, Object> context = velocityParamFactory.getDefaultVelocityParams(MapBuilder.<String, Object>newBuilder()
                                                                                                          .add("project", project)
                                                                                                          .add(POST_FUNCTIONS_KEY_NAME, getHipChatPostFunctions(project))
                                                                                                          .add(HAS_HIP_CHAT_API_TOKEN, hasHipChatApiToken())
                                                                                                          .toMap(),
                                                                                                  authenticationContext
                );

                // The content-type needs to be set so that Sitemesh will decorate responses generated by Velocity correctly.
                response.setContentType("text/html; charset=UTF-8");

                renderer.render("templates/hip-chat-notifications.vm", context, response.getWriter());
            }
        }
    }

    private Collection<SimpleHipChatPostFunction> getHipChatPostFunctions(final Project project)
    {
        Map<String, String> roomIdNameMap = getRooms();

        final Map<FunctionDescriptor, Collection<String>> issueTypeMap = new HashMap<FunctionDescriptor, Collection<String>>();
        final Collection<SimpleHipChatPostFunction> postFunctions = new ArrayList<SimpleHipChatPostFunction>();

        final Map<String, String> workflowMap = workflowSchemeManager.getWorkflowMap(project);
        for (Map.Entry<String, String> issueTypeWorkflow : workflowMap.entrySet())
        {
            final String issueTypeId = issueTypeWorkflow.getKey();
            final String issueTypeName = (issueTypeId == null) ? getI18n().getText("common.words.default") : issueTypeManager.getIssueType(issueTypeId).getName();
            final String workflowName = issueTypeWorkflow.getValue();
            final JiraWorkflow workflow = workflowManager.getWorkflow(workflowName);
            final Map<ActionDescriptor, Collection<FunctionDescriptor>> postFunctionsForWorkflow = workflowManager.getPostFunctionsForWorkflow(workflow);

            for (Map.Entry<ActionDescriptor, Collection<FunctionDescriptor>> actionPostFunction : postFunctionsForWorkflow.entrySet())
            {
                int postFunctionCount = 0;
                final ActionDescriptor actionDescriptor = actionPostFunction.getKey();
                for (FunctionDescriptor functionDescriptor : actionPostFunction.getValue())
                {
                    postFunctionCount++;
                    final Map<String, String> args = functionDescriptor.getArgs();
                    if (HipChatPostFunction.class.getName().equals(args.get(CLASS_NAME_ARG_KEY)))
                    {
                        final String notifyClients = args.get(HipChatPostFunctionFactory.NOTIFY_CLIENTS_PARAM);
                        final String roomsToNotifyCsvIds = args.get(HipChatPostFunctionFactory.ROOMS_TO_NOTIFY_CSV_IDS_PARAM);
                        final String jql = args.get(HipChatPostFunctionFactory.JQL_FILTER_PARAM);
                        final String message = getNotificationMessage(args.get(HipChatPostFunctionFactory.MESSAGE_FILTER_PARAM));

                        Collection<String> issueTypeNames = issueTypeMap.get(functionDescriptor);
                        if (issueTypeNames == null)
                        {
                            issueTypeNames = new ArrayList<String>();
                            issueTypeNames.add(issueTypeName);
                            issueTypeMap.put(functionDescriptor, issueTypeNames);

                            final Collection<String> roomsToNotify = getRoomsToNotify(roomIdNameMap, roomsToNotifyCsvIds);

                            postFunctions.add(new SimpleHipChatPostFunction(issueTypeNames, workflowName, workflow.getMode(), actionDescriptor.getName(),
                                                                            notifyClients, roomsToNotify, jql, message, postFunctionCount));
                        }
                        else
                        {
                            issueTypeNames.add(issueTypeName);
                        }
                    }
                }
            }
        }

        return postFunctions;
    }

    private String getNotificationMessage(final String message)
    {
        try
        {
            return messageRenderer.renderNotification(message, new DummyNotificationDto(authenticationContext, applicationProperties, issueTypeManager));
        }
        catch (final IOException e)
        {
            // Leave the message in raw form
            return message;
        }
    }

    private Map<String, String> getRooms()
    {
        Map<String, String> roomIdNames = new HashMap<String, String>();
        final Either<ClientError, List<Room>> rooms = hipChatClient.rooms().list().claim();
        if (rooms.isRight())
        {
            for (Room room : rooms.right().get())
            {
                roomIdNames.put(Long.toString(room.getId()), room.getName());
            }
        }
        return roomIdNames;
    }

    private Collection<String> getRoomsToNotify(final Map<String, String> roomIdNameMap, final String roomsToNotifyCsvIds)
    {
        Collection<String> roomsToNotify = new ArrayList<String>();
        Iterable<String> roomsToNotifyIds = Splitter.on(",").omitEmptyStrings().split(Strings.nullToEmpty(roomsToNotifyCsvIds));
        for (String roomId : roomsToNotifyIds)
        {
            final String roomName = roomIdNameMap.get(roomId);
            if (roomName != null)
            {
                roomsToNotify.add(roomName);
            }
            else
            {
                roomsToNotify.add(roomId);
            }
        }
        return roomsToNotify;
    }

    private boolean hasHipChatApiToken()
    {
        final String hipChatApiToken = configurationManager.getHipChatApiToken();
        return hipChatApiToken != null && !hipChatApiToken.equals("");
    }

    private I18nHelper getI18n()
    {
        return authenticationContext.getI18nHelper();
    }

    public static class SimpleHipChatPostFunction
    {
        private final Collection<String> issueTypeNames;
        private final String workflowName;
        private final String workflowMode;
        private final String actionName;
        private final String notifyClients;
        private final Collection<String> roomsToNotify;
        private final String jql;
        private final String messageWithHtml;
        private final int count;

        public SimpleHipChatPostFunction(final Collection<String> issueTypeNames, final String workflowName, final String workflowMode, final String actionName,
                                         final String notifyClients, final Collection<String> roomsToNotify, final String jql, final String messageWithHtml, final int count)
        {

            this.issueTypeNames = issueTypeNames;
            this.workflowName = workflowName;
            this.workflowMode = workflowMode;
            this.actionName = actionName;
            this.notifyClients = notifyClients;
            this.roomsToNotify = roomsToNotify;
            this.jql = jql;
            this.count = count;
            this.messageWithHtml = messageWithHtml;
        }

        public Collection<String> getIssueTypeNames()
        {
            return issueTypeNames;
        }

        public String getWorkflowName()
        {
            return workflowName;
        }

        public String getActionName()
        {
            return actionName;
        }

        public String getNotifyClients()
        {
            return notifyClients;
        }

        public Collection<String> getRoomsToNotify()
        {
            return roomsToNotify;
        }

        public String getJql()
        {
            return jql;
        }

        public int getCount()
        {
            return count;
        }

        public String getWorkflowMode()
        {
            return workflowMode;
        }

        public String getMessageWithHtml()
        {
            return messageWithHtml;
        }
    }
}
